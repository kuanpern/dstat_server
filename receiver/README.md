
### TODO:
* test snooze functionality
* use external databases

### Documentation
```
usage: receiver_main.py [-h] --conf CONF --specs SPECS

DSTAT generic receiver and alert manager

optional arguments:
  -h, --help     show this help message and exit
  --conf CONF    main configuration file
  --specs SPECS  server specs configuration file
```

