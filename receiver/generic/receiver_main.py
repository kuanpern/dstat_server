import os
import time
import datetime
import threading
import argparse
import yaml
import socket
import requests
import pandas as pd
import numpy as np
import logging
import logging.handlers

import opends
import opends.easymail

def send_alert_email(msg, configs):
	logger.info('sending email')
	response = opends.easymail.send_email(
		emailUser = configs['sender_email_addr'],
		api_key   = configs['email_api_key'],
		recipient = configs['recv_email_addr'],
		subject   = 'computing resource alert',
		text      = str(msg),
	) # end email
# end def

def parse_dstat_dat(dat):
	timestamp = dat['epoch']['epoch']
	mem_usage = dat['memory usage']['used'] / sum(dat['memory usage'].values())
	mem_usage = mem_usage*100
	cpu_usage = 100 - dat['total cpu usage']['idl']
	return {'timestamp': timestamp, 'mem_usage': mem_usage, 'cpu_usage': cpu_usage}
# end def

def get_recent_usage_pct(url, password, timelimit=900, timeout=1):
	stime = time.time() - timelimit

	# query data
	r = requests.post(url, json={'password': password}, timeout=timeout)
	data = r.json()['data']

	# disk usage
	data_disk = data['disk']
	df_disk = pd.DataFrame(data_disk)
	# - time filter
	_df = df_disk.sort_values(by='timestamp')
	_df = _df[_df['timestamp'] > stime]
	disk_usage = np.mean(_df['usage'].values)

	# cpu and memory usage
	data_dstat = data['dstat']
	df_dstat = pd.DataFrame(list(map(parse_dstat_dat, data_dstat)))
	# - time filter
	_df = df_dstat.sort_values(by='timestamp')
	_df = _df[_df['timestamp'] > stime]
	cpu_usage = np.mean(_df['cpu_usage'].values)
	mem_usage = np.mean(_df['mem_usage'].values)
	
	return {'disk': disk_usage, 'mem': mem_usage, 'cpu': cpu_usage}
# end def

def ping_server(url, password, timeout=1):
	r = requests.post(url, json={'password': password}, timeout=timeout)
	return r.json()
# end def

def manage_alert(name, msg, email_configs):
	msg_out = name+':'+msg
	send_alert_email(msg_out, email_configs)
# end def


def watch_server(server_configs):
	name = server_configs['name']
	url  = server_configs['url']
	password  = server_configs['password']
	timelimit = server_configs['timelimit']
	_alerts = server_configs['alerts']
	disk_limit = _alerts['disk']
	mem_limit  = _alerts['mem']
	cpu_limit  = _alerts['cpu']
	heartbeat  = _alerts['heartbeat']
	snooze     = _alerts['snooze']

	logger.info('watching server: %s' % (name,))

	# initial parameters value
	last_notification_time = 0
	last_contact = 0
	limits = {'disk': disk_limit, 'mem': mem_limit, 'cpu': cpu_limit}

	# url formatting
	if not(url.endswith('/')):
		url = url.strip()+'/'
	# end if

	while True:
		# resource checking
		cur_time = time.time()
		_interval = cur_time - last_notification_time
		if _interval < snooze:
			logger.info('still in snooze period, not checking')
			time.sleep(snooze - _interval + 10)
			continue
		# end if

		logger.info('query server: %s' % (name,))
		try:
			usages = get_recent_usage_pct(url=url+'dstat', password=password, timelimit=timelimit)
			for key in limits.keys():
				if usages[key] > limits[key]:
					msg = '%s usage (%4.1f) exceeds threshold (%4.1f)' % (key, usages['disk'], disk_limit,)
					manage_alert(name, msg, email_configs)
					last_notification_time = cur_time
				# end if
			# end for
		except Exception as e:
			logger.error(repr(e))
		# end try

		# ping checking
		try:
			r = ping_server(url=url+'heartbeat', password=password)
			last_contact = time.time()
			logger.info(str(r))
		except Exception as e:
			logger.warning('server %s does not answer to ping' % (name,))
			logger.warning(repr(e))
		# end try
		_interval = time.time() - last_contact
		if _interval > heartbeat:
			logger.warning('server %s does not answer to ping' % (name,))
			msg = '%s uncontactable for at least %4.1f seconds' % (name, _interval,)
			manage_alert(name, msg, email_configs)
			last_notification_time = cur_time
		# end if

		time.sleep(ping_interval) # TODO: adjustable ?
	# end while
# end def

####################################
######## READ CONFIGURATIONS #######
####################################
parser = argparse.ArgumentParser(description='DSTAT generic receiver and alert manager')
parser.add_argument('--conf',  help="main configuration file", type=str, required=True)
parser.add_argument('--specs', help="server specs configuration file", type=str, required=True)
pars = vars(parser.parse_args())

# read main configuration
main_config_file = pars['conf']
with open(main_config_file, 'rb') as fin:
	main_configs = yaml.load(fin)
# end with
ping_interval      = main_configs['ping_interval']
main_logfile       = main_configs['log_file']
email_api_key_file = main_configs['email_api_key_file']
sender_email_addr  = main_configs['sender_email_addr']
recv_email_addr    = main_configs['recv_email_addr']
with open(email_api_key_file) as fin:
	email_api_key = fin.read().strip()
# end with
email_configs = {
  'sender_email_addr': main_configs['sender_email_addr'],
  'recv_email_addr'  : main_configs['recv_email_addr'],
  'email_api_key'    : email_api_key,
} # end configs


# read servers' config
server_config_file = pars['specs']
with open(server_config_file) as fin:
	server_configs = yaml.load(fin)
# end with

####################################
########### LOGGING SETUP ##########
####################################
_logdir = os.sep.join(main_logfile.split(os.sep)[:-1])
if not os.path.exists(_logdir):
    os.makedirs(_logdir)
# end if
handler = logging.handlers.TimedRotatingFileHandler(main_logfile, when='midnight')
format_str = '%(asctime)s - %(levelname)-8s - %(message)s'
date_format = '%Y-%m-%d %H:%M:%S'
formatter = logging.Formatter(format_str, date_format)
handler.setFormatter(formatter)
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(handler)


# start threads to watch servers
n = len(server_configs)
for i in range(n):
	logger.info('starting thread [%d]' % (i+1,))
	configs = server_configs[i]
	task = threading.Thread(target=watch_server, args=(configs,))
	task.start()
	time.sleep(ping_interval/(n+1))
# end for

