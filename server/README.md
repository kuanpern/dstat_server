### To build the docker image,
```
$ sudo docker build -t kuanpern/dstat_server:latest .
```


### To run the server, do
```
$ sudo docker run -dt --name dstat  --restart unless-stopped \
  -p 7770:80 \
 kuanpern/dstat_server:latest \
  --refresh 86400 \
  --average 10 \
  --maxfiles 1000 \
  --drefresh midnight \
  --dinterval 300 \
  --dmaxfiles 1000 \
  --password foobar
```
all arguments are optional. (note we choose 7770 as web portal port here)

### Querying statistics
The docker exposes two endpoints, "/heartbeat" and "/dstat" for resources query
* /heartbeat returns current resource usage, e.g. 
```
$ curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"password":"foobar"}' \
  http://localhost:7770/heartbeat
>> {'authentication': 'success', 'cpu_pct': 5.5, 'disk_avail': 184953752, 'hostname': 0, 'memory_pct': 40.3, 'timestamp': '2019-01-21T13:40:39.232307'}
```
* /dstat returns historical resource usage, 
```
$ curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"password":"foobar"}' \
  http://localhost:7770/dstat
>> {'authentication': 'success',
 'data': [{'dsk/total': {'read': 0.0, 'writ': 44646.4},
   'epoch': {'epoch': 1548078108.858},
   'memory usage': {'buff': 275480576.0,
    'cach': 2576289792.0,
    'free': 2658848768.0,
    'used': 2723606528.0},
   'net/total': {'recv': 0.0, 'send': 0.0},
   'paging': {'in': 0.0, 'out': 0.0},
   'procs': {'blk': 0.0, 'new': 0.0, 'run': 0.0},
   'system': {'csw': 1839.5, 'int': 438.6},
   'total cpu usage': {'hiq': 0.0,
    'idl': 97.501,
    'siq': 0.038,
    'sys': 0.502,
    'usr': 1.758,
    'wai': 0.201}}, ...]}
```

see tests/dstat_server_query_example.ipynb for an example

