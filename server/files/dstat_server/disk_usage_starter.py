import os
import time
import json
import argparse
import logging
import logging.handlers

parser = argparse.ArgumentParser(description='Node disk space statistic generator')
parser.add_argument('--refresh',  help="file refresh interval",  default='midnight', type=str)
parser.add_argument('--interval', help="interval to query (secs)", default=300,	  type=int)
pars = vars(parser.parse_args())

interval = pars['interval']
refresh  = pars['refresh']

logfile = '/root/disk_logs/disk_usage.log'

####################################
########### LOGGING SETUP ##########
####################################
handler = logging.handlers.TimedRotatingFileHandler(logfile, when=refresh)
format_str = '%(asctime)s - %(levelname)-8s - %(message)s'
date_format = '%Y-%m-%d %H:%M:%S'
formatter = logging.Formatter(format_str, date_format)
handler.setFormatter(formatter)
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(handler)

while True:
	s = os.statvfs('/')
	usage = 1 - s.f_bavail / s.f_blocks
	usage = usage * 100
	dat = {'timestamp': time.time(), 'usage': usage}
	logger.info('@ ' + json.dumps(dat))
	time.sleep(interval)
# end while



