import time
import os
import argparse
import shlex
import subprocess
import glob
import logging

parser = argparse.ArgumentParser(description='Node resource statistics generator')
parser.add_argument('--refresh',   help="refresh interval (secs)",   default=86400, type=int)
parser.add_argument('--average',   help="averaging interval (secs)", default=10,	  type=int)
parser.add_argument('--drefresh',  help="file refresh interval for disk usage query",  default='midnight', type=str)
parser.add_argument('--dinterval', help="interval to query (secs) for disk usage query", default=300,	  type=int)
parser.add_argument('--maxfiles',  help="maximum number of log files", default=1000,  type=int)
parser.add_argument('--dmaxfiles',  help="maximum number of log files for disk usage query", default=1000,  type=int)
parser.add_argument('--password',  help="server password", default="foobar", type=str, required=False)
pars = vars(parser.parse_args())

os.chdir('/root')

# start web server
cmd = 'venv/bin/python3 dstat_server/dstat_pub_server.py --password {PASSWORD}'
cmd = cmd.format(PASSWORD=pars['password'])
print(cmd)
proc_server = subprocess.Popen(shlex.split(cmd))

# start disk usage logger
cmd = 'venv/bin/python3 dstat_server/disk_usage_starter.py --refresh {REFRESH} --interval {INTERVAL}'
cmd = cmd.format(INTERVAL=pars['dinterval'], REFRESH=pars['drefresh'])
print(cmd)
proc_disk = subprocess.Popen(shlex.split(cmd))


# the dstat command line
cmd_temp = '/usr/bin/dstat -Ta --noupdate --noheader --vmstat --output {outfilename} {avesec} > /dev/null &'

# note: hardcoded
dstat_logdir = '/root/dstat_logs/'
disk_logdir  = '/root/disk_logs/'

c = 0
while True:
	c += 1
	_now = str(int(time.time()))
	basename = 'dstat-'+_now+'.log'
	filename = dstat_logdir + os.sep + basename

	logging.info('cycle %d starts. Logging to %s' % (c, filename))
	cmd = cmd_temp.format(
		outfilename = filename,
		avesec      = pars['average']
	)
	p = subprocess.Popen(cmd, shell=True)
	time.sleep(pars['refresh'])
	p.terminate()

	# keep number of files to a limit (dstat)
	logfiles = glob.glob(dstat_logdir+'/dstat-*.log')
	logfiles.sort()
	logfiles.reverse()
	for i in range(pars['maxfiles'], len(logfiles)):
		os.remove(logfiles[i])
	# end for

	# keep number of files to a limit (disk)
	logfiles = glob.glob(disk_logdir+'/*.log')
	logfiles.sort()
	logfiles.reverse()
	for i in range(pars['dmaxfiles'], len(logfiles)):
		os.remove(logfiles[i])
	# end for

# end while
